# Epicode Books
Gestione di catalago di una biblioteca

##Specifiche

La libreria gestisce nel catalogo libri e periodici.

Per ogni _libro_ � necessario gestire:
- Titolo
- Autori(possono essere pi� di uno)
- Editore
- Data di pubblicazione
- Codice ISBN (se presente)
- Codice  interno della biblioteca
- Numero di copie a disposizione
- Volumi in cui � strutturato
- Lingua
- Categoria di classificazione (anche piu di una)
- Numero di pagine totali ( di tutti i volumi)

Ogni _copia_ a disposizione ha:
- Una posizione nella biblioteca
- Una indicazione che comunica se disponibile per il prestito o meno
- Uno stato ( in consultazione, in prestito, disponibile)

Ogni _volume_ ha:
- Titolo
- Codice ISBN (se presente)
- Codice  interno della biblioteca
- Numero di pagine

Ogni _Autore_ � caratterizzato da:
- Nome
- Cognome
- Nazionalit�
- Anno di nascita
- Anno di morte(se non in vita)

I _periodici_ sono caratterizzati da:
- Data di pubblicazione
- Editore
- Titolo
- Categoria di classificazione (anche pi� di una)
- Periodicit� (quotidiano, mensile, annuale,estemporanea, ecc)

##Permessi

Gli operatori della biblioteca possonmo gestire il catalogo, mentre gli utenti possono solo fare delle ricerche.












