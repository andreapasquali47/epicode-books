package it.andreapasquali.services;

import java.util.List;

import it.andreapasquali.models.Libro;
import it.andreapasquali.models.Volume;


public interface VolumeService {

	public List<Volume> getByTitolo(String titolo);
	public List<Volume> getByCodiceISBN(Long codiceISBN);
	public List<Volume> getByCodiceInterno(int codiceInterno);
	public List<Volume> getByNumeroPagine(int numeroPagine);
	public List<Volume> getByLibro(Libro libro);
	public Volume salva(Volume volume);
	public Volume update(Volume volume);
	public Volume delete(Volume volume);
}
