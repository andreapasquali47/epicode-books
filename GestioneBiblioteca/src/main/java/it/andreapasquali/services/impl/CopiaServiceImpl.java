package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Copia;
import it.andreapasquali.models.Volume;
import it.andreapasquali.models.types.Stato;
import it.andreapasquali.repository.CopiaRepository;
import it.andreapasquali.services.CopiaService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class CopiaServiceImpl implements CopiaService{

	@Autowired
	CopiaRepository copiaR;
	
	@Override
	public List<Copia> getByPosizione(String posizione) {
		try {
			return copiaR.findByPosizione(posizione);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Copia> getByStato(Stato stato) {
		try {
			return copiaR.findByStato(stato);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Copia> getByVolume(Volume volume) {
		try {
			return copiaR.findByVolume(volume);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Copia salva(Copia copia) {
		try {
			return copiaR.save(copia);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Copia update(Copia copia) {
		try {
			Copia c = copiaR.findById(copia.getId()).get();
			if(copia.getPosizione() != null) if(copia.getPosizione().isBlank()) c.setPosizione(copia.getPosizione());
			if(copia.isDisponibile()) c.setDisponibile(copia.isDisponibile());
			if(copia.getStato() != null) c.setStato(copia.getStato());
			if(copia.getVolume() != null) c.setVolume(copia.getVolume());
			
			return copiaR.save(c);
			} catch (Exception e) {
				throw new AppServiceException(e);
			}
	}

}
