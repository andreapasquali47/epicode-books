package it.andreapasquali.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Categoria;
import it.andreapasquali.repository.CategoriaRepository;
import it.andreapasquali.services.CategoriaService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class CategoriaServiceImpl implements CategoriaService {

	@Autowired
	CategoriaRepository categoriaR;

	@Override
	public Categoria getByCategoria(String categoria) {
		try {
			return categoriaR.findByCategoria(categoria);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Categoria salva(Categoria categoria) {
		try {
			return categoriaR.save(categoria);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}
	@Override
	public Categoria update(Categoria categoria) {
		try {
			Categoria c = categoriaR.findById(categoria.getId()).get();
			if(categoria.getCategoria() != null) if(!categoria.getCategoria().isBlank()) c.setCategoria(categoria.getCategoria());
			return categoriaR.save(c);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
