package it.andreapasquali.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Editore;
import it.andreapasquali.repository.EditoreRepository;
import it.andreapasquali.services.EditoreService;
import it.andreapasquali.services.exception.AppServiceException;
@Service
public class EditoreServiceImpl implements EditoreService {

	@Autowired
	EditoreRepository editoreR;
	
	@Override
	public Editore getByNome(String nome) {
		try {
			return editoreR.findByNome(nome);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Editore salva(Editore editore) {
		try {
			return editoreR.save(editore);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
