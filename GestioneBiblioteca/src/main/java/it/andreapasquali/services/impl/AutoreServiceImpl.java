package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Autore;
import it.andreapasquali.repository.AutoreRepository;
import it.andreapasquali.services.AutoreService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class AutoreServiceImpl implements AutoreService {

	@Autowired
	AutoreRepository autoreR;

	@Override
	public List<Autore> getByNome(String nome) {
		try {
			return autoreR.findByNome(nome);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Autore> getByCognome(String cognome) {
		try {
			return autoreR.findByCognome(cognome);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Autore> getByNomeAndCognome(String nome, String cognome) {
		try {
			return autoreR.findByNomeAndCognome(nome, cognome);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Autore salva(Autore autore) {
		try {
			return autoreR.save(autore);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
		
	}

	@Override
	public Autore update(Autore autore) {
		try {
		Autore a = autoreR.findById(autore.getId()).get();
		if(autore.getNome() != null) {
			 if(!autore.getNome().isBlank())
				a.setNome(autore.getNome());
		}
		if(autore.getCognome() != null) if(!autore.getCognome().isBlank()) a.setCognome(autore.getCognome());
		if(autore.getNazionalita() != null) if(!autore.getNazionalita().isBlank()) a.setNazionalita(autore.getNazionalita());
		if(autore.getAnnoNascita() != null) a.setAnnoNascita(autore.getAnnoNascita());
		if(autore.getAnnoMorte() != null) a.setAnnoMorte(autore.getAnnoMorte());
		
		return autoreR.save(a);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}


	
	
}
