package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Libro;
import it.andreapasquali.repository.LibroRepository;
import it.andreapasquali.services.LibroService;
import it.andreapasquali.services.exception.AppServiceException;

@Service
public class LibroServiceImpl implements LibroService {

	@Autowired
	LibroRepository libroR;
	
	
	public Libro delete(Libro libro) {
		var u = libroR.getById(libro.getId());
		try {
			libroR.delete(u);
			return u;
		} catch(Exception e) {
			throw new AppServiceException(e);
		}
	}

	public Libro salva(Libro libro) {
		try {
			return libroR.save(libro);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Libro update(Libro libro) {
		try {
			Libro b = libroR.findById(libro.getId()).get();
			if(libro.getTitolo() != null) if(libro.getTitolo().isBlank()) b.setTitolo(libro.getTitolo());
			if(libro.getEditore() != null)  b.setEditore(libro.getEditore());
			if(libro.getDataDiPubblicazione() != null) b.setDataDiPubblicazione(libro.getDataDiPubblicazione());
			if(libro.getCodiceISBN() != null) b.setCodiceISBN(libro.getCodiceISBN());
			if(libro.getNumCopieDisponibili() != 0) b.setNumCopieDisponibili(libro.getNumCopieDisponibili());
			if(libro.getNumInternoBiblioteca() != 0) b.setNumInternoBiblioteca(libro.getNumInternoBiblioteca());
			if(libro.getNumeroPagineTot() != 0) b.setNumeroPagineTot(libro.getNumeroPagineTot());
			if(libro.getLingua() != null) b.setLingua(libro.getLingua());
			if(libro.getCategoria() != null) b.setCategoria(libro.getCategoria());
			return libroR.save(b);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}


	@Override
	public List<Libro> getByTitolo(String titolo) {
		try {
			return libroR.findByTitolo(titolo);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Libro> getByCodiceISBN(Long codiceIsbn) {
		try {
			return libroR.findByCodiceISBN(codiceIsbn);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	
	


	

}
