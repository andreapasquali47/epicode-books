package it.andreapasquali.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Lingua;
import it.andreapasquali.repository.LinguaRepository;
import it.andreapasquali.services.LinguaService;
import it.andreapasquali.services.exception.AppServiceException;
@Service
public class LinguaServiceImpl implements LinguaService{

	@Autowired
	LinguaRepository linguaR;
	@Override
	public Lingua getByNome(String lingua) {
		try {
			return linguaR.findByLingua(lingua);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Lingua salva(Lingua lingua) {
		try {
			return linguaR.save(lingua);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

}
