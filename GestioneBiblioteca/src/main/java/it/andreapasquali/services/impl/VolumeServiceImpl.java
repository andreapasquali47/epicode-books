package it.andreapasquali.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.andreapasquali.models.Libro;
import it.andreapasquali.models.Volume;
import it.andreapasquali.repository.VolumeRepository;
import it.andreapasquali.services.VolumeService;
import it.andreapasquali.services.exception.AppServiceException;
@Service
public class VolumeServiceImpl implements VolumeService {

	@Autowired
	VolumeRepository volumeR;

	@Override
	public List<Volume> getByTitolo(String titolo) {
		try {
			return volumeR.findByTitolo(titolo);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Volume> getByCodiceISBN(Long codiceISBN) {
		try {
			return volumeR.findByCodiceISBN(codiceISBN);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Volume> getByCodiceInterno(int codiceInterno) {
		try {
			return volumeR.findByCodiceInterno(codiceInterno);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Volume> getByNumeroPagine(int numeroPagine) {
		try {
			return volumeR.findByNumeroPagine(numeroPagine);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Volume> getByLibro(Libro libro) {
		try {
			return volumeR.findByLibro(libro);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Volume salva(Volume volume) {
		try {
			return volumeR.save(volume);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Volume update(Volume volume) {
		try {
			Volume v = volumeR.findById(volume.getId()).get();
			if(volume.getTitolo() != null) if(volume.getTitolo().isBlank()) v.setTitolo(volume.getTitolo());
			if(volume.getCodiceISBN() != null) v.setCodiceISBN(volume.getCodiceISBN());
			if(volume.getCodiceInterno() != 0) v.setCodiceInterno(volume.getCodiceInterno());
			if(volume.getNumeroPagine() != 0) v.setNumeroPagine(volume.getNumeroPagine());
			if(volume.getLibro() != null) v.setLibro(volume.getLibro());
			return volumeR.save(v);	
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Volume delete(Volume volume) {
		// TODO Auto-generated method stub
		return null;
	}
}
