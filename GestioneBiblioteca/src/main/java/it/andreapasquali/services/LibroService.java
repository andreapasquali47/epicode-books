package it.andreapasquali.services;

import java.util.List;

import it.andreapasquali.models.Libro;

public interface LibroService {

	public Libro delete(Libro libro);
	public Libro salva(Libro libro);
	public Libro update(Libro libro);
	public List<Libro> getByTitolo(String titolo);
	public List<Libro> getByCodiceISBN(Long codiceIsbn);
}
