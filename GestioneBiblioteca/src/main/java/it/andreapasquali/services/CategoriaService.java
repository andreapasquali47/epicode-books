package it.andreapasquali.services;

import it.andreapasquali.models.Categoria;

public interface CategoriaService {

	public Categoria getByCategoria(String categoria);
	public Categoria salva(Categoria categoria);
	public Categoria update(Categoria categoria);
}
