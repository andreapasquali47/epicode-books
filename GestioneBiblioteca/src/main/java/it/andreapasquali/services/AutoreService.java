package it.andreapasquali.services;

import java.time.LocalDate;
import java.util.List;

import it.andreapasquali.models.Autore;

public interface AutoreService {

	public List<Autore> getByNome(String nome);
	public List<Autore> getByCognome(String cognome);
	public List<Autore> getByNomeAndCognome(String nome, String cognome);
	public Autore salva(Autore autore);
	public Autore update(Autore autore);
	//Autore salva(Autore autore, int anno, int mese, int giorno);
}
