package it.andreapasquali.services;

import it.andreapasquali.models.Lingua;

public interface LinguaService {

	public Lingua getByNome(String lingua);
	public Lingua salva(Lingua lingua);
}
