package it.andreapasquali.services;

import java.util.List;

import it.andreapasquali.models.Copia;
import it.andreapasquali.models.Volume;
import it.andreapasquali.models.types.Stato;

public interface CopiaService {

	public List<Copia> getByPosizione(String posizione);
	public List<Copia> getByStato(Stato stato);
	public List<Copia> getByVolume(Volume volume);
	public Copia salva(Copia copia);
	public Copia update(Copia copia);
	
}
