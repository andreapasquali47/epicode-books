package it.andreapasquali.services;

import it.andreapasquali.models.Editore;

public interface EditoreService {

	public Editore getByNome(String nome);
	public Editore salva(Editore editore);
}
