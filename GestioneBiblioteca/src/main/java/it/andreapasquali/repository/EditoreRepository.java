package it.andreapasquali.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Editore;

@Repository
public interface EditoreRepository extends JpaRepository<Editore, Integer>{

	public Editore findByNome(String nome);

}
