package it.andreapasquali.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Lingua;
@Repository
public interface  LinguaRepository extends JpaRepository<Lingua, Integer>{

	public Lingua findByLingua(String lingua);
}
