package it.andreapasquali.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Libro;
@Repository
public interface LibroRepository extends JpaRepository<Libro, Integer>{

	public List<Libro> findByTitolo(String titolo);
	public List<Libro> findByCodiceISBN(long ISBN);
}
