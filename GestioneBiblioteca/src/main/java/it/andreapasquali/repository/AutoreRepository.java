package it.andreapasquali.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Autore;

@Repository
public interface AutoreRepository extends JpaRepository<Autore, Integer>{

	public List<Autore> findByNome(String nome);
	public List<Autore> findByCognome(String cognome);
	public List<Autore> findByNomeAndCognome(String nome, String cognome);
}
