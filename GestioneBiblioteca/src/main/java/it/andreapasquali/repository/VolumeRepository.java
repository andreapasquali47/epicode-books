package it.andreapasquali.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Libro;
import it.andreapasquali.models.Volume;

@Repository
public interface VolumeRepository extends JpaRepository<Volume, Integer>{

	public List<Volume> findByTitolo(String titolo);
	public List<Volume> findByLibro(Libro libro);
	public List<Volume> findByNumeroPagine(int numeroPagine);
	public List<Volume> findByCodiceInterno(int codiceInterno);
	public List<Volume> findByCodiceISBN(Long codiceISBN);

}
