package it.andreapasquali.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Copia;
import it.andreapasquali.models.Volume;
import it.andreapasquali.models.types.Stato;
@Repository
public interface CopiaRepository extends JpaRepository<Copia, Integer>{

	public List<Copia> findByPosizione(String posizione);
	public List<Copia> findByStato(Stato stato);
	public List<Copia> findByVolume(Volume volume);

	
}
