package it.andreapasquali.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.andreapasquali.models.Categoria;
@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Integer>{

	public Categoria findByCategoria(String categoria);
}
