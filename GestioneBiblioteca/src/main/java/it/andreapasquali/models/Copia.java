package it.andreapasquali.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import it.andreapasquali.entities.BaseEntity;
import it.andreapasquali.models.types.Stato;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
@Builder
public class Copia extends BaseEntity {

	private String posizione;
	private boolean disponibile;
	@Enumerated(EnumType.STRING)
	private Stato stato;
	@ManyToOne
	private Volume volume;

}
