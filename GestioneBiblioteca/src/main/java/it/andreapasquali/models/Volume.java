package it.andreapasquali.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import it.andreapasquali.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
@Builder
public class Volume extends BaseEntity{

	private String titolo;
    private Long codiceISBN;
    private int codiceInterno;
    private int numeroPagine;
    @ManyToOne
    private Libro libro;
	
}
