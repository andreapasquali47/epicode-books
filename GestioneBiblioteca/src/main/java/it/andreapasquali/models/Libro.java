package it.andreapasquali.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import it.andreapasquali.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
@Builder
public class Libro extends BaseEntity {

	private String titolo;
	@ManyToOne
	private Editore editore;
	private LocalDate dataDiPubblicazione;
	private Long codiceISBN;
	private int numCopieDisponibili;
	private int numInternoBiblioteca;
	@ManyToOne
	private Lingua lingua;
	@ManyToMany
	@JoinTable(
            name="libro_categoria",
            joinColumns= @JoinColumn(name="libro_id", referencedColumnName="id"),
            inverseJoinColumns= @JoinColumn(name="categoria_id", referencedColumnName="id")
        )
	private Set<Categoria> categoria;
	private int numeroPagineTot;
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinTable(
            name="libro_autore",
            joinColumns = @JoinColumn(name="libro_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="autore_id", referencedColumnName="id")
        )
	private Set<Autore> autore;
	
	
	
}
