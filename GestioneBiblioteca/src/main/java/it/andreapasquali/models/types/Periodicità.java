package it.andreapasquali.models.types;

public enum Periodicità {

		MENSILE,SETTIMANALE,BIMESTRALE,TRIMESTRALE,ANNUALE
}
