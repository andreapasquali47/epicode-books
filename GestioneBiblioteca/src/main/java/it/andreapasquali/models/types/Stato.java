package it.andreapasquali.models.types;

public enum Stato {
	
	IN_CONSULTAZIONE, IN_PRESTITO, DISPONIBILE
}
