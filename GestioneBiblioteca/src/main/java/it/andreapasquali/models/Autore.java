package it.andreapasquali.models;

import java.time.LocalDate;

import javax.persistence.Entity;

import it.andreapasquali.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
@Builder
public class Autore extends BaseEntity {

	private String nome;
	private String cognome;
	private String nazionalita; 
	private LocalDate annoNascita;
	private LocalDate annoMorte;
	
}
