package it.andreapasquali.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import it.andreapasquali.entities.BaseEntity;
import it.andreapasquali.models.types.Periodicità;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(callSuper = true)
@Builder
public class Periodico extends BaseEntity {

	private LocalDate DataPubblicazione;
	@ManyToOne
	private Editore editore;
	private String titolo;
	@ManyToMany
	@JoinTable(
            name="periodico_categoria",
            joinColumns= @JoinColumn(name="periodico_id", referencedColumnName="id"),
            inverseJoinColumns= @JoinColumn(name="categoria_id", referencedColumnName="id")
        )
	private Set<Categoria> categoria;
	@Enumerated(EnumType.STRING)
	private Periodicità periodicità;

}
