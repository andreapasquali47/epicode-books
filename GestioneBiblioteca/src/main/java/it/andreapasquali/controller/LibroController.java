package it.andreapasquali.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.Libro;
import it.andreapasquali.services.impl.LibroServiceImpl;

@RestController
@RequestMapping("/libro")
public class LibroController {

	@Autowired
	LibroServiceImpl libroSI;
	/*
	@GetMapping(value = "/deletelibro")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Libro deleteLibro(@RequestParam int id) {
		Libro libro = libroSI.getById(id).get();
		return libroSI.delete(libro);
	}
	@GetMapping(value = "/addlibro")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Libro addLibro(@RequestBody Libro libro) {
		return libroSI.add(libro);
	}
	
	@GetMapping(value = "/updatelibro")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Libro updateLibro(@RequestBody Libro libro) {
		return libroSI.update(libro);
	}*/
}
