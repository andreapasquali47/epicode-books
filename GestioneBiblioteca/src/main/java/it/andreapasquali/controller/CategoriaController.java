package it.andreapasquali.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.Categoria;
import it.andreapasquali.services.impl.CategoriaServiceImpl;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {

	@Autowired
	CategoriaServiceImpl categoriaSI;
	
	@GetMapping("/getbycategoria")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Categoria getByCategoria(@RequestParam String categoria){
		return categoriaSI.getByCategoria(categoria);
	}
	@PostMapping("/salva")
	public Categoria salva(@RequestBody Categoria categoria){
		return categoriaSI.salva(categoria);
	}
	@PostMapping("/update")
	public Categoria update(@RequestBody Categoria categoria){
		return categoriaSI.update(categoria);
	}
}







