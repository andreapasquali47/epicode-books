package it.andreapasquali.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.andreapasquali.models.Autore;
import it.andreapasquali.services.impl.AutoreServiceImpl;

@RestController
@RequestMapping("/autore")
public class AutoreController {

	@Autowired
	AutoreServiceImpl autoreSI;
	
	@GetMapping("/getbynome")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Autore> getByNome(@RequestParam String nome){
		return autoreSI.getByNome(nome);
	}
	@GetMapping("/getbycognome")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Autore> getByCognome(@RequestParam String cognome){
		return autoreSI.getByCognome(cognome);
	}
	@GetMapping("/getbynomeandcognome")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Autore> getByNomeAndCognome(@RequestParam String nome, @RequestParam String cognome){
		return autoreSI.getByNomeAndCognome(nome, cognome);
	}
	@PostMapping("/salva")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Autore salva(@RequestBody Autore autore) {// @RequestParam int anno, @RequestParam int mese, @RequestParam int giorno){
		//LocalDate date = LocalDate.of(anno, mese, giorno);
		//autore.setAnnoNascita(date);
		return autoreSI.salva(autore);
	}
	@PostMapping("/update")
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Autore update(@RequestBody Autore autore){// @RequestParam int anno, @RequestParam int mese, @RequestParam int giorno) {
		//LocalDate dataNascita = LocalDate.of(anno, mese, giorno);
		//LocalDate dataMorte = LocalDate.of(anno, mese, giorno);
		//autore.setAnnoNascita(dataNascita);
		//autore.setAnnoMorte(dataMorte);
		return autoreSI.update(autore);
	}
	
}
