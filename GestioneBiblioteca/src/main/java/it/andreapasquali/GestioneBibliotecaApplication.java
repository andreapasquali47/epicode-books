package it.andreapasquali;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneBibliotecaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneBibliotecaApplication.class, args);
	}

}
